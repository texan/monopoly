﻿using System.Collections.Generic;

namespace Monopoly
{
    public class Property
    {
        public short Houses { get; set; }
        public bool InMonopoly { get; set; }
        public string Name { get; }
        public bool Ownable { get; }
        public Player Owner { get; }
        public int Price { get; }
        public List<int> Rent { get; }
        public short Space { get; }
        public string Set { get; }

        public Property(short space, string name, string set, int price, List<int> rent, bool ownable)
        {
            Space = space;
            Name = name;
            Set = set;
            Price = price;
            Rent = rent;
            Ownable = ownable;
            Owner = null;
            InMonopoly = false;
            Houses = 0;
        }

        // Outputs how many houses are currently on the property
        public string writeHouses()
        {
            switch (Houses)
            {
                case 1: return "  *  ";
                case 2: return "  ** ";
                case 3: return " *** ";
                case 4: return " ****";
                case 5: return "*****";
                default: return "     ";
            }
        }

        public string writeOwner() => Owner.Name;
    }
}
